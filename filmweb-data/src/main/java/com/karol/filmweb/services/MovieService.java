package com.karol.filmweb.services;

import com.karol.filmweb.data.ActorRepository;
import com.karol.filmweb.data.MovieRepository;

import com.karol.filmweb.domain.Actor;
import com.karol.filmweb.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private ActorRepository actorRepository;

    public List<Movie> getAllMovies() {
        List<Movie> movies = new ArrayList<>();
        movieRepository.findAll()
                .forEach(movies::add);
        return movies;
    }

    public Movie getMovie(String id) {
        return movieRepository.findById(id).orElse(null);
    }

    public void addMovie(Movie movie) {
        movieRepository.save(movie);
    }

    public void updateMovie(Movie movie) {
        movieRepository.save(movie);
    }

    public void addActor(String movieId, Actor actor) {
        Movie movie = movieRepository.findById(movieId).orElse(null);
        movie.getActors().add(actor);
        movieRepository.save(movie);
        actorRepository.save(actor);

    }

    public List<Actor> getAlMovieActors(String movieId) {
        Movie movie = movieRepository.findById(movieId).orElse(null);
        System.out.println(movie.getActors().get(1).getFirstName());
        return movie.getActors();
    }
}
