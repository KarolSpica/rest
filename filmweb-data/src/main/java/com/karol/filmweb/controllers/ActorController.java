package com.karol.filmweb.controllers;

import com.karol.filmweb.domain.Actor;
import com.karol.filmweb.domain.Movie;
import com.karol.filmweb.services.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ActorController {

    @Autowired
    private ActorService actorService;

    @RequestMapping(method = RequestMethod.POST,value = "/actors")
    public void addActor(@RequestBody Actor actor) {
        actorService.addActor(actor);
    }

    @RequestMapping("/actors")
    public List<Actor> getAllActors() {
        return actorService.getAllActors();
    }

    @RequestMapping("/actors/{actorId}")
    public Actor getActorById(@PathVariable long actorId) {
        return actorService.getActorById(actorId);
    }

    @RequestMapping("actors/{actorId}/movies")
    public List<Movie> getAllActorMovies(@PathVariable long actorId) {
        return actorService.getAllActorMovies(actorId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/actors/{actorId}")
    public void addMovieToActor(@RequestBody Movie movie, @PathVariable long actorId) {
        actorService.addActorMovie(actorId, movie);
    }
}
