package com.karol.filmweb.controllers;

import com.karol.filmweb.domain.Actor;
import com.karol.filmweb.domain.Movie;
import com.karol.filmweb.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    private MovieService movieService;

    @RequestMapping("/movies")
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @RequestMapping("/movies/{movieId}")
    public Movie getMovie(@PathVariable String movieId) {
        return movieService.getMovie(movieId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/movies")
    public void addMovie(@RequestBody Movie movie) {
        movieService.addMovie(movie);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/movies/{movieId}")
    public void updateMovie(@RequestBody Movie movie, @PathVariable String movieId) {
        movieService.updateMovie(movie);
    }

    @RequestMapping(method = RequestMethod.POST, value = "movies/{movieId}/actors")
    public void addActor(@RequestBody Actor actor, @PathVariable String movieId) {
        movieService.addActor(movieId,actor);
    }

    @RequestMapping("movies/{movieId}/actors")
    public List<Actor> getAllMovieActors(@PathVariable String movieId) {
      return movieService.getAlMovieActors(movieId);
    }

}

